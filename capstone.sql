-- 1.
SELECT customerName FROM customers WHERE country = "Philippines";

-- 2.
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3.
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

-- 4.
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- 5.
SELECT customerName FROM customers WHERE state IS NULL;

-- 6.
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

-- 7.
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- 8.
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- 9.
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 10.
SELECT DISTINCT country FROM customers;

-- 11.
SELECT DISTINCT status FROM orders;

-- 12.
SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");

-- 13.
SELECT firstName, lastName, city FROM employees
	JOIN offices ON employees.officeCode = offices.officeCode
	WHERE offices.officeCode = 5;

-- 14.
SELECT customerName FROM customers
	JOIN offices ON customers.salesRepEmployeeNumber = employees.employeeNumber
	WHERE employees.employeeNumber = 1166;

-- 15.
SELECT productName, customerName FROM customers
	JOIN orders ON customers.customerNumber = orders.customerNumber
	JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
	JOIN products ON orderdetails.productCode = products.productCode
	WHERE customers.customerNumber = 121;

-- 16.
SELECT DISTINCT employees.firstName, employees.lastName, customers.customerName, offices.country FROM customers
	JOIN offices ON customers.country = offices.country
	JOIN employees ON offices.officeCode = employees.officeCode;

-- 17.
SELECT productName, quantityInStock FROM products
	WHERE productLine = "Planes" AND quantityInStock < 1000;

-- 18.
SELECT customerName FROM customers WHERE phone LIKE "+81%";